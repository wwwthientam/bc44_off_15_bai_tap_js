function myFunction() {
  var loaiKH = document.getElementById("txt-loai-kh").value;
  if (loaiKH == "Doanh-nghiep") {
    document.getElementById(
      "so-ket-noi"
    ).innerHTML = ` <label for="">Số kết nối</label>
      <input id="txt-so-ket-noi" type="number" class="form-control" />`;
  } else {
    document.getElementById("so-ket-noi").innerHTML = `
      <label style="display:none" for="">Số kết nối</label>
      <input id="txt-so-ket-noi" style="display:none" type="number" class="form-control" />
    
  `;
  }
}
function Submit() {
  var maKH = document.getElementById("txt-ma-kh").value;
  if (maKH == "") {
    alert("Vui Lòng Nhập Mã Khách Hàng!");
    return false;
  }
  var loaiKH = document.getElementById("txt-loai-kh").value;
  var soketnoi = document.getElementById("txt-so-ket-noi").value * 1;
  var kenhcaocap = document.getElementById("txt-kenh-cao-cap").value * 1;
  var tiencap = 0;
  if (loaiKH == "Doanh-nghiep" && soketnoi < 11) {
    tiencap = 15 + 75 + 50 * kenhcaocap;
  } else if (soketnoi >= 11) {
    tiencap = 15 + 75 + (soketnoi - 10) * 5 + 50 * kenhcaocap;
  } else {
    tiencap = 4.5 + 20.5 + 7.5 * kenhcaocap;
  }

  document.getElementById("result").innerHTML = `
<h5> Mã Khách Hàng: ${maKH}.</h5>
<h5>Tiền Cáp Phải Thanh Toán: ${tiencap.toLocaleString()} $</h5>
    `;
}
