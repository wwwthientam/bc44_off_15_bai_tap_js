function Submit() {
  var name = document.getElementById("txt-name").value;
  var thunhap = document.getElementById("txt-tong-thu-nhap").value * 1;
  var songuoi = document.getElementById("txt-so-nguoi-phu-thuoc").value * 1;
  var thunhapchiuthue = thunhap - 4 - songuoi * 1.6;
  var thuephaidong = 0;
  if (thunhapchiuthue <= 60) {
    thuephaidong = thunhapchiuthue * 0.05;
  } else if (thunhapchiuthue <= 120) {
    thuephaidong = thunhapchiuthue * 0.1;
  } else if (thunhapchiuthue <= 210) {
    thuephaidong = thunhapchiuthue * 0.15;
  } else if (thunhapchiuthue <= 384) {
    thuephaidong = thunhapchiuthue * 0.2;
  } else if (thunhapchiuthue <= 624) {
    thuephaidong = thunhapchiuthue * 0.25;
  } else if (thunhapchiuthue <= 960) {
    thuephaidong = thunhapchiuthue * 0.3;
  } else {
    thuephaidong = thunhapchiuthue * 0.35;
  }
  document.getElementById("result").innerHTML = `
  <h5>Tên người khai nộp thuế: ${name}</h5>
  <h5>Thuế thu nhập cá nhân phải đóng: ${thuephaidong} triệu VNĐ </h5>
  `;
}
