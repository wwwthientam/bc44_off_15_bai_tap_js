function Submit() {
  var tenchuho = document.getElementById("txt-name").value;
  var luongdien = document.getElementById("txt-luong-dien").value * 1;
  var tiendien = 0;
  if (luongdien <= 50) {
    tiendien = 500 * luongdien;
  } else if (luongdien <= 100) {
    tiendien = 500 * 50 + (luongdien - 50) * 650;
  } else if (luongdien <= 200) {
    tiendien = 500 * 50 + 650 * 50 + (luongdien - 100) * 850;
  } else if (luongdien <= 350) {
    tiendien = 500 * 50 + 650 * 50 + 100 * 850 + (luongdien - 200) * 1100;
  } else {
    tiendien =
      500 * 50 + 650 * 50 + 100 * 850 + 150 * 1100 + (luongdien - 350) * 1300;
  }
  document.getElementById("result").innerHTML = `
  <h3> Tên chủ hộ: ${tenchuho} </h3>
  <h3> Tiền điện bạn phải trả: ${tiendien.toLocaleString()} VND </h3>
  `;
}
